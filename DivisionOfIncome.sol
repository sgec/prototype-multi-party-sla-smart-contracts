pragma solidity ^0.4.25;

import "./StakeholdersConsensus.sol";

contract DivisionOfIncome is StakeholdersConsensus {

    constructor(address _endUserAddress) public {
        endUserAddress = _endUserAddress;
        owner = msg.sender;
    }

    // the price for each stakeholder is determined by VC system owner
    function determineStakeholdersPrice(address serviceAddress, bytes32 serviceName, uint pricePHour) public {
        require(msg.sender == owner);
        require(serviceName.length > 0 && stakeholders[serviceAddress].pricePerHour == 0);
        require(pricePHour > 0);
        stakeholders[msg.sender].pricePerHour = pricePHour;
        stakeholderIndex[stakeholderCount] = serviceAddress;
        stakeholderCount++;
        emit StakeholderPriceNotification(serviceAddress, pricePHour);
    }
}