pragma solidity ^0.4.25;

import "./DynamicPrice.sol";

contract StakeholdersConsensus is DynamicPrice {

    mapping(address => Stakeholder) stakeholders;
    mapping(uint => address) stakeholderIndex;
    bool consensusDetermined = false;

    uint stakeholderCount = 0;

    event StakeholderPriceNotification(address _stakeholderAddress, uint _pricePHour);

    // custom data structure to hold locked funds and time
    struct Stakeholder {
        uint pricePerHour;
        bytes32 name;   // descriptive
    }

    constructor(address _endUserAddress) public {
        require(_endUserAddress != address(0));

        owner = msg.sender;
        price = 0;
        durationSeconds = 0;
        endUserAddress = _endUserAddress;
    }

    // pay the max amount and lock funds
    function payVCService() public payable {
        require(consensusDetermined == true);
        super.payVCService();
    }

    // the price for each stakeholder is determined by each stakeholder
    function determineStakeholdersPrice(bytes32 serviceName, uint pricePHour) public {
        require(serviceName.length > 0 && stakeholders[msg.sender].pricePerHour == 0);
        require(pricePHour > 0);
        stakeholders[msg.sender].pricePerHour = pricePHour;
        stakeholders[msg.sender].name = serviceName;
        stakeholderIndex[stakeholderCount] = msg.sender;
        stakeholderCount++;
        emit StakeholderPriceNotification(msg.sender, pricePHour);
    }

    function consensusFinished(uint ownerPrice, uint _durationSeconds){
        require(msg.sender == owner);
        consensusDetermined = true;
        price = 0;
        price += ownerPrice;
        durationSeconds = _durationSeconds;

        for (uint i = 0; i < stakeholderCount; i++)
        {
            price += stakeholders[stakeholderIndex[i]].pricePerHour / 3600 * durationSeconds;
        }
    }

    // check if user has funds due for pay out because lock time is over
    function triggerReleaseTime() {
        require(msg.sender == endUserAddress);
        require(accounts[msg.sender].isFinished != true);

        // split funds if there is remaining time
        if (accounts[msg.sender].releaseTime < now) {
            uint currentTime = now;
            uint timeElapsed = accounts[msg.sender].releaseTime - currentTime;
            uint gweiReturn = timeElapsed * (price / durationSeconds);


            for (uint i = 0; i < stakeholderCount; i++)
            {
                uint gweiStakeholderReturn = stakeholders[stakeholderIndex[i]].pricePerHour / 3600 * timeElapsed;
                stakeholderIndex[i].send(gweiStakeholderReturn);
            }

            msg.sender.send(gweiReturn);
            accounts[msg.sender].balance -= gweiReturn;
            accounts[msg.sender].isFinished = true;
            emit FundsReleaseEvent(releaseTime, lockTimeS, currentTime, price, gweiReturn);
        }
    }
}