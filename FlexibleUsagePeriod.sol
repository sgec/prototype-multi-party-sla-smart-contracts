pragma solidity ^0.4.25;

import "./DynamicPrice.sol";

contract FlexibleUsagePeriod is DynamicPrice {

    constructor(uint _price, uint _durationSeconds, address _endUserAddress, uint _incentivePrice) DynamicPrice(_price, _durationSeconds, _endUserAddress) public {
        incentivePrice = _incentivePrice;
    }
}