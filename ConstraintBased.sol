pragma solidity ^0.4.25;

import "./DynamicPrice.sol";

contract ConstraintBased is DynamicPrice {

    mapping(uint => Restriction) restrictions;
    uint restrictionCounter = 0;
    bool restrictionsSet = false;

    struct Restriction {
        bytes32 rule;
        address restrictionOwner;
        bytes32 legalAgreement;
    }

    event StakeholderPriceNotification(address _owner, bytes32 _rule, bytes32 _agreement);

    constructor(address _endUserAddress) public {
        price = 0;
        durationSeconds = 0;

        owner = msg.sender;
        endUserAddress = _endUserAddress;
    }

    function addRestriction(bytes32 _rule, bytes32 _legalAgreement, bool isLastRestriction) public {
        require(msg.sender == endUserAddress);
        require(_rule.length > 0 && _legalAgreement.length > 0);

        restrictions[restrictionCounter].rule = _rule;
        restrictions[restrictionCounter].restrictionOwner = msg.sender;
        restrictions[restrictionCounter].legalAgreement = _legalAgreement;

        restrictionCounter++;
        if (isLastRestriction == true) {
            restrictionsSet = true;
            emit StakeholderPriceNotification(msg.sender, restrictions[restrictionCounter].rule, restrictions[restrictionCounter].legalAgreement);
        }
    }

    function determinePrice(uint _price) public {
        require(msg.sender == owner);
        require(price == 0);
        price = _price;
    }

    function getRestrictionRule(uint index)public returns(bytes32 rule) {
        return restrictions[index].rule;
    }
}